package selection

import (
	"github.com/stretchr/testify/assert"
	_ "log"
	"testing"
)

func TestRandSelection(t *testing.T) {
	array := []int{4, 2, 3, 7, 1, 8, 6, 5}
	// q := partition(array, 0, len(array)-1)
	q := array[partitionSelect(array, 0, len(array)-1, 3)]
	assert.True(t, q == 4, "should be 4, but is %d", q)
}
