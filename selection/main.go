// Package main provides selection algorithms and a function to test them
package selection

import (
	"fmt"
	"math/rand"
)

func partitionSelect(array []int, p, r, i int) int {
	fmt.Println(array)
	fmt.Println(p, r, i)
	q := partition(array, p, r)
	k := q - p + 1
	if k == i {
		return array[q]
	}
	if k > i {
		return partitionSelect(array, p, q-1, i)
	}
	return partitionSelect(array, q+1, r, i-k) //0 based index
}

func partition(array []int, p, r int) int {
	pivot := rand.Intn(r-p+1) + p // +1 because Intn returns [0,n),and we want r-p be returned
	key := array[pivot]
	array[pivot], array[r] = array[r], array[pivot]
	i := p - 1
	for j := p; j < r; j += 1 {
		if array[j] < key { //swap i and j, remark upper bound
			i += 1
			array[i], array[j] = array[j], array[i]
		}
	}
	fmt.Println(array)
	i += 1
	array[i], array[r] = array[r], array[i]
	return i
}
