// Package rbtree provides red-black tree implementation
package rbtree

import (
	"fmt"
	"log"
)

type Color int

const (
	BLACK = 0
	RED   = 1
)

type TreeNode struct {
	parent *TreeNode
	lchild *TreeNode
	rchild *TreeNode
	color  Color
	key    float64
}

type RbTree struct {
	root     *TreeNode
	sentinel *TreeNode
}

func (rbt *RbTree) NewNode(key float64) *TreeNode {
	node := new(TreeNode)
	node.parent = rbt.sentinel
	node.lchild = rbt.sentinel
	node.rchild = rbt.sentinel
	node.key = key
	node.color = RED
	return node
}

func NewRbTree() *RbTree {
	sentinel := new(TreeNode)
	sentinel.color = BLACK
	sentinel.parent = sentinel
	sentinel.lchild = sentinel
	sentinel.rchild = sentinel
	sentinel.key = 0
	return &RbTree{sentinel: sentinel, root: sentinel}
}

func (rbt *RbTree) LeftRotate(node *TreeNode) {
	log.Println("left rotation, key is ", node.key)
	if node.rchild == rbt.sentinel {
		return
	}
	rChild := node.rchild
	rChild.parent = node.parent
	node.rchild = rChild.lchild
	rChild.lchild.parent = node
	if node == node.parent.lchild {
		node.parent.lchild = rChild
	} else {
		node.parent.rchild = rChild
	}
	node.parent = rChild
	rChild.lchild = node
	if rChild.parent == rbt.sentinel {
		rbt.root = rChild
	}
}

func (rbt *RbTree) RightRotate(node *TreeNode) {
	log.Println("right rotation, key is ", node.key)
	if node.lchild == rbt.sentinel {
		return
	}
	lChild := node.lchild
	lChild.parent = node.parent
	node.lchild = lChild.rchild
	lChild.rchild.parent = node
	if node == node.parent.lchild {
		node.parent.lchild = lChild
	} else {
		node.parent.rchild = lChild
	}
	node.parent = lChild
	lChild.rchild = node
	if lChild.parent == rbt.sentinel {
		rbt.root = lChild
	}
}

func (rbt *RbTree) Insert(key float64) {
	newNode := rbt.NewNode(key)
	parent := rbt.sentinel
	current := rbt.root
	for current != rbt.sentinel {
		parent = current
		if key > current.key {
			current = current.rchild
		} else {
			current = current.lchild
		}
	}
	// now we are at leaf
	newNode.parent = parent
	if parent == rbt.sentinel {
		rbt.root = newNode
	} else if newNode.key > parent.key {
		parent.rchild = newNode
	} else {
		parent.lchild = newNode
	}
	// both children are already set to sentinel and color set to RED in NewNode()
	rbt.InsertFixup(newNode)
}

func (rbt *RbTree) InsertFixup(node *TreeNode) {
	log.Println("fixing node", node.key, "; node color is ", node.color, "node parent is ", node.parent.key)
	for node.parent.color == RED {
		log.Println("paretn is red")
		if node.parent == node.parent.parent.lchild {
			log.Println("parent is lchild")
			uncle := node.parent.parent.rchild
			if uncle.color == RED {
				node.parent.color = BLACK
				uncle.color = BLACK
				node.parent.parent.color = RED
				node = node.parent.parent
			} else {
				if node == node.parent.rchild {
					node = node.parent
					rbt.LeftRotate(node)
				}
				node.parent.color = BLACK
				node.parent.parent.color = RED
				rbt.RightRotate(node.parent.parent)
			}
		} else {
			log.Println("parent is rchild")
			uncle := node.parent.parent.lchild
			if uncle.color == RED {
				node.parent.color = BLACK
				uncle.color = BLACK
				node.parent.parent.color = RED
				node = node.parent.parent
			} else {
				if node == node.parent.lchild {
					node = node.parent
					rbt.RightRotate(node)
				}
				node.parent.color = BLACK
				node.parent.parent.color = RED
				rbt.LeftRotate(node.parent.parent)
			}
		}
	}
	rbt.root.color = BLACK
	return
}

func (rbt *RbTree) PrintNode(node *TreeNode) {
	if node == rbt.sentinel {
		return
	}
	fmt.Println("node key is: ", node.key, "; color is ", node.color)
	rbt.PrintNode(node.lchild)
	rbt.PrintNode(node.rchild)
}
