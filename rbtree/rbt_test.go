// Package rbtree provides red-black tree
package rbtree

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"log"
	"testing"
)

func TestNewRbtree(t *testing.T) {
	rbt := NewRbTree()
	assert.True(t, rbt.root == rbt.sentinel,
		"root should be equal to sentinel")
}

func TestRotate(t *testing.T) {
	rbt := NewRbTree()
	n1 := rbt.NewNode(1)
	n2 := rbt.NewNode(2)
	n3 := rbt.NewNode(3)
	n4 := rbt.NewNode(4)
	n5 := rbt.NewNode(5)
	n6 := rbt.NewNode(6)
	n7 := rbt.NewNode(7)

	rbt.root = n4
	n4.lchild = n2
	n4.rchild = n6
	n4.parent = rbt.sentinel
	n4.color = BLACK
	n2.parent = n4
	n2.lchild = n1
	n2.rchild = n3
	n6.parent = n4
	n6.lchild = n5
	n6.rchild = n7
	n1.parent = n2
	n3.parent = n2
	n5.parent = n6
	n7.parent = n6
	n1.color = BLACK
	n3.color = BLACK
	n5.color = BLACK
	n7.color = BLACK

	t.Log(rbt.root.key)
	fmt.Println(rbt.root.key)

	rbt.LeftRotate(n2)
	fmt.Println(rbt.root.lchild.key)
	assert.True(t, rbt.root.lchild.key == 3, "rotation failed")

	rbt.RightRotate(n6)
	assert.True(t, rbt.root.rchild.key == 5, "rotation failed")
}

func TestInsert(t *testing.T) {
	log.Println("start testing insertion")
	rbt := NewRbTree()
	rbt.Insert(1)
	rbt.Insert(2)
	rbt.Insert(3)
	rbt.Insert(4)
	rbt.Insert(5)
	rbt.Insert(6)
	rbt.Insert(7)
	rbt.PrintNode(rbt.root)
}
